# Kubernetes


### Setup steps

1) Start machines

We need 4 machines: t3a.xlarge. Start these and give them appropriate tags:
- ansible
- k8 control panel
- k8 agent a
- k8 agent b
  


2) Allow SG networking

All machines:
- Allow cohort9_homeips
- Allow Jenkins
  

Ansible will have to connect to k8 machines to set up.
- Allow ansible into those machines; we can just add a group that allows cohort9_homeips


3) Create and share key pairs

In the ansible machine:
- Create a new key pair

```bash
$ ssh-keygen 
> Add a path/name
> No password
```

Log into our k8 machines:
- Add/append the public k8 key to the authorized_keys file
- Test steps 3 and 4 by trying to log in to each k8 machine via the ansible machine

4) Clone kubesprays to ansible machine

```bash
$ git clone https://github.com/kubernetes-sigs/kubespray.git
```

Then
```bash
$ cd kubespray
$ sudo apt update
$ sudo apt install python3-pip -y
```

Run the requirements.txt
```bash
sudo pip3 install -r requirements.txt
```

Note: this repo should install ansible but may need these dependencies:

```bash
# python3
# git
```


5) Edit inventory file in Kubespray and other setup

Kubespray has a simple inventory file that we can copy and edit:

```bash
# Copy ``inventory/sample`` as ``inventory/mycluster``
cp -rfp inventory/sample inventory/mycluster
```

In the inventory file, add your k8 private IPs and specify the ssh key:

```bash
$ cd inventory/mycluster 
$ nano inventory.ini
```

```YAML
# Copy of your inventory yaml from my cluster here
# Notes in book
ansible_ssh_private_key_file=/home/ubuntu/.ssh/asses_4_key ansible_user=ubuntu at the end of nodes 1, 2 and 3
edit pvt IPs of nodes
uncomment 1 of next block
uncomment all three of the block after that
uncomment 2 and 3 of block after that
```

Then do this:
```bash
# Update Ansible inventory file with inventory builder
declare -a IPS=(10.10.1.3 10.10.1.4 10.10.1.5) #pvt IPs of instances

CONFIG_FILE=inventory/mycluster/hosts.yaml python3 contrib/inventory_builder/inventory.py ${IPS[@]}

sudo apt install ansible -y

ansible-playbook -i ~/kubespray/inventory/mycluster/inventory.ini --become-user=root ~/kubespray/cluster.yml -b  --private-key=~/.ssh/k8_key.pem
# Review and change parameters under ``inventory/mycluster/group_vars``
cat inventory/mycluster/group_vars/all/all.yml
cat inventory/mycluster/group_vars/k8s_cluster/k8s-cluster.yml
```

The ssh key can also be specified when calling the ansible playbook.


6) Run playbook

ansible-playbook -i ~/kubespray/inventory/mycluster/inventory.ini --become-user=root ~/kubespray/cluster.yml -b  --private-key=~/.ssh/k8_key.pem

7) Ssh into control panel and run the following commands:

```bash
$ sudo cp /etc/kubernetes/admin.conf $HOME/ & sudo chown $(id -u):$(id -g) $HOME/admin.conf & export KUBECONFIG=$HOME/admin.conf
# Do with && if it doesn't work and if that doesn't work just do the three commands individually
# Test they've worked by doing:
kubectl get ns
```



### Deployment files

A Deployment provides declarative updates for Pods and ReplicaSets. You describe a desired state in a Deployment, and the Deployment Controller changes the actual state to the desired state at a controlled rate. You can define Deployments to create new ReplicaSets, or to remove existing Deployments and adopt all their resources with new Deployments. 

A Kubernetes deployment is a resource object in Kubernetes that provides declarative updates to applications. A deployment allows you to describe an application’s life cycle, such as which images to use for the app, the number of pods there should be, and the way in which they should be updated. 

Deployments represent a set of multiple, identical Pods with no unique identities. A Deployment runs multiple replicas of your application and automatically replaces any instances that fail or become unresponsive. In this way, Deployments help ensure that one or more instances of your application are available to serve user requests. Deployments are managed by the Kubernetes Deployment controller. Deployments use a Pod template, which contains a specification for its Pods. The Pod specification determines how each Pod should look: what applications should run inside its containers, which volumes the Pods should mount, its labels, and more. When a Deployment's Pod template is changed, new Pods are automatically created one at a time.
Link to pod template: https://cloud.google.com/kubernetes-engine/docs/concepts/pod#pod-templates
Link to pod specification: https://v1-20.docs.kubernetes.io/docs/reference/generated/kubernetes-api/v1.20/#podspec-v1-core

The deployment file is written in yaml. Example of a deployment:

apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.14.2
        ports:
        - containerPort: 80


#### Sections of a deployment 

Deployments must contain:
- apiVersion (apps/v1)
- Kind (Deployment)
- The metadata with a spec section to define the replicas and configurations related to the deployment


Here (above), a Deployment named nginx-deployment is created, indicated by the .metadata.name field.

The Deployment creates three replicated Pods, indicated by the .spec.replicas field.

The .spec.selector field defines how the Deployment finds which Pods to manage. In this case, you select a label that is defined in the Pod template (app: nginx). However, more sophisticated selection rules are possible, as long as the Pod template itself satisfies the rule.


The template field contains the following sub-fields:

- The Pods are labeled app: nginxusing the .metadata.labels field.
- The Pod template's specification, or .template.spec field, indicates that the Pods run one container, nginx, which runs the nginx Docker Hub image at version 1.14.2.
- Create one container and name it nginx using the .spec.template.spec.containers[0].name field.


#### Labels


Kubernetes has a lot of nifty built-in features. Kubernetes labels are one of them. Kubernetes labels allow DevOps teams to identify Kubernetes objects and organize them into groups. One good use-case of this is grouping pods based on the application they belong to. Teams can also develop any number of labeling conventions including ones to group pods by environment, customer, team/owner or release version.

But that is just the start; Kubernetes labels pack a lot of other functionality. For example, it is possible to do bulk operations in Kubectl, by filtering a large number of resources, using Kubernetes labels. Kubernetes deployments also use labels to identify the pods that they manage. Similarly, Kubernetes services and replication controllers use labels to refer to a set of pods. Recommended Kubernetes labels also support querying and interoperability between Kubernetes tools.

#### Pods

Pods run the containers which contain the images.

#### K8 Control Panel

The K8 Control Panel is where we can define services, deployments and generally describe the desired state of these in the k8 worker nodes.

General terminology:

- `kubectl`
- pods
- k8 nodes
- namespaces
- deployment files
- services

In your k8 control panel you can interact with you cluster mainly via `kubectl`- the kubernete cli that makes things happen.


#### Starting the service

One you start your cluster, you'll need to run:

```bash
# Run to start the kubectl service
$sudo cp /etc/kubernetes/admin.conf $HOME/ && sudo chown $(id -u):$(id -g) $HOME/admin.conf && export KUBECONFIG=$HOME/admin.conf
```

Then you can do `kubectl get ns` to check that it's up and running.

#### Namespaces

A section area that you can then deploy services and pods into. Like a VPC but inside the nodes. 

```bash
$ kubectl get ns

$ kubectl create ns

$ kubectl delete ns <namespace name>
```


#### Deployment and sevices

These are the two main sections that we'll be working on. They are a declaritive way to deploy pods into the cluster and create networking "services" 


#### Node ports

A service that exposes a deployment (i.e. a bunch of pods) but only to a specific agent. Exposes an IP to a resource inside a node. Not great


#### Loadbalancer



#### Externalname


#### Ingress control

Allows the outside world to connect to specific services or pods via Ingress rules. E.g. if you are coming via the DNS PetFuneral.academyal.com you end up in the correct website/pods/service. If you are coming via PetClinic.academyal.com you will end up in a different pod with the correct PetClinic website. 

We'll install HA proxy Ingress controller:

https://github.com/haproxytech/kubernetes-ingress


Step 1: in our control panel

```bash
$ git clone https://github.com/haproxytech/kubernetes-ingress.git
```

Step 2: Apply the deployment of the ingress controller

```bash
kubectl apply -f deploy/haproxy-ingress.yaml
```


####  Commands

Delete a namespace: ```kubectl delete namespaces <name of namespace>```

Run deployment: ```kubectl apply -f nginx-deployment.yaml -n nginx```
'-n' is for namespace, this needs to be specified

Check which pods are running: ```kubectl get pods --field-selector=status.phase=Running```
```kubectl get pods -n nginx```

Kill a pod: ```kubectl delete pod <name of pod>```
In example above, three have been made so when you delete a pod, a new one will be made which is why three will still be there when you check on the pods

Decribe a pod: ```kubectl describe pods <name of pod> -n nginx``
Some of info you get: node, start time, labels, annotations, IP info  

To ssh into a pod (and into a container): 
```kubectl exec --stdin --tty <name of pod> -n <name of deployment> -- /bin/bash ```



Task- create a deployment:

```bash
# Create a namespace named nginx
kubectl create ns nginx

# Create your deployment file
nano nginx-deployment.yaml

# This will open the file ready for you to input text:
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.14.2
        ports:
        - containerPort: 80
# Save and exit

# Run deployment. Need to specify namespace otherwise it'll use the default one
kubectl apply -f nginx-deployment.yaml -n nginx

# See how many pods are running
kubectl get pods -n nginx
# You will see three here since that's how many were created in the deployment file

# You can delete a pod if you like
kubectl delete pod <name of pod>
# Then when you use the get pods command, there will still be three since a new one is automatically created when one is deleted

# Ssh into pod
kubectl exec --stdin --tty <pod name> -n <namespace name> -- /bin/bash
```
